﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace RandomGeneration {
    class Program {
        static void Main(string[] args) {
            new Program();
        }

        private Random random;

        /*
         * 0 = nothing
         * 1 = room
         * 2 = roof
         * 3 = balcony? (not implemented yet)
         */

        private int[][][] floors;
        private int size;
        private int entrances;
        private int breakableWalls;

        public Program() {
            random = new Random();
            initArray(8, 8, 3);
            entrances = generate(2, 4);
            breakableWalls = generate(2, 4);
            int type = generateType();
            type = 0;

            if (type == 0) {
                size = generate(6, 10);
                generateHouse();
            } else if (type == 1) {
                generateWarehouse();
            } else if (type == 2) {
                generateMultiStoryHouse();
            }

            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
        }

        /* 0 = normal house
         * 1 = warehouse
         * 2 = multi story house (2 floors)
         */

        public int generateType() {
            int type = generate(0, 2);
            return type;
        }

        /*
         * Generates Standard House
         */

        public void generateHouse() {
            int randomint = 0;
            if (size == 6) {
                if (generate(0, 1) == 0) {
                    for (int i = 0; i < floors.Length; i++) {
                        for (int j = 0; j < floors[0].Length; j++) {
                            if (i < 2 && j < 3) {
                                floors[i][j][0] = 1;
                                floors[i][j][1] = 2;
                            }
                        }
                    }
                } else {
                    for (int i = 0; i < floors.Length; i++) {
                        for (int j = 0; j < floors[0].Length; j++) {
                            if (i < 3 && j < 2) {
                                floors[i][j][0] = 1;
                                floors[i][j][1] = 2;
                            }
                        }
                    }
                }
            } else if (size == 7) {
                if (generate(0, 1) == 0) {
                    for (int i = 0; i < floors.Length; i++) {
                        for (int j = 0; j < floors.Length; j++) {
                            if (i < 2 && j < 3) {
                                floors[i][j][0] = 1;
                                floors[i][j][1] = 2;
                            }
                        }
                    }
                    floors[2][1][0] = 1;
                    floors[2][1][1] = 2;
                } else {
                    for (int i = 0; i < floors.Length; i++) {
                        for (int j = 0; j < floors[0].Length; j++) {
                            if (i < 3 && j < 2) {
                                floors[i][j][0] = 1;
                                floors[i][j][1] = 2;
                            }
                        }
                    }
                    floors[1][2][0] = 1;
                    floors[1][2][1] = 2;
                }
            } else if (size == 8) {
                randomint = generate(0, 2);
                if (randomint == 0) {
                    for (int i = 0; i < floors.Length; i++) {
                        for (int j = 0; j < floors[0].Length; j++) {
                            if (i < 4 && j < 2) {
                                floors[i][j][0] = 1;
                                floors[i][j][1] = 2;
                            }
                        }
                    }
                } else if (randomint == 1) {
                    for (int i = 0; i < floors.Length; i++) {
                        for (int j = 0; j < floors[0].Length; j++) {
                            if (i < 2 && j < 4) {
                                floors[i][j][0] = 1;
                                floors[i][j][1] = 2;
                            }
                        }
                    }
                } else {
                    for (int i = 0; i < floors.Length; i++) {
                        for (int j = 0; j < floors[0].Length; j++) {
                            if (i < 2 && j < 3) {
                                floors[i][j][0] = 1;
                                floors[i][j][1] = 2;
                            }
                        }
                    }
                    floors[2][0][0] = 1;
                    floors[2][0][1] = 2;
                    floors[2][1][0] = 1;
                    floors[2][1][1] = 2;
                }
            } else if (size == 9) {
                randomint = generate(0, 2);
                if (randomint == 0) {
                    for (int i = 0; i < floors.Length; i++) {
                        for (int j = 0; j < floors[0].Length; j++) {
                            if (i < 3 && j < 3) {
                                floors[i][j][0] = 1;
                                floors[i][j][1] = 2;
                            }
                        }
                    }
                } else if (randomint == 1) {
                    for (int i = 0; i < floors.Length; i++) {
                        for (int j = 0; j < floors[0].Length; j++) {
                            if (i < 4 && j < 2) {
                                floors[i][j][0] = 1;
                                floors[i][j][1] = 2;
                            }
                        }
                    }
                    randomint = generate(0, 1);
                    floors[randomint * 3][2][0] = 1;
                    floors[randomint * 3][2][1] = 2;
                } else if (randomint == 2) {
                    for (int i = 0; i < floors.Length; i++) {
                        for (int j = 0; j < floors[0].Length; j++) {
                            if (i < 2 && j < 4) {
                                floors[i][j][0] = 1;
                                floors[i][j][1] = 2;
                            }
                        }
                    }
                    randomint = generate(0, 1);
                    floors[2][randomint * 3][0] = 1;
                    floors[2][randomint * 3][1] = 2;
                }
            } else if (size == 10) {
                randomint = generate(0, 5);
                if (randomint == 0) {
                    for (int i = 0; i < floors.Length; i++) {
                        for (int j = 0; j < floors[0].Length; j++) {
                            if (i < 5 && j < 2) {
                                floors[i][j][0] = 1;
                                floors[i][j][1] = 2;
                            }
                        }
                    }
                } else if (randomint == 1) {
                    for (int i = 0; i < floors.Length; i++) {
                        for (int j = 0; j < floors[0].Length; j++) {
                            if (i < 2 && j < 5) {
                                floors[i][j][0] = 1;
                                floors[i][j][1] = 2;
                            }
                        }
                    }
                } else if (randomint == 2) {
                    for (int i = 0; i < floors.Length; i++) {
                        for (int j = 0; j < floors[0].Length; j++) {
                            if (i < 4 && j < 2) {
                                floors[i][j][0] = 1;
                                floors[i][j][1] = 2;
                            }
                        }
                    }
                    floors[1][2][0] = 1;
                    floors[1][2][1] = 2;
                    floors[2][2][0] = 1;
                    floors[2][2][1] = 2;
                } else if (randomint == 3) {
                    for (int i = 0; i < floors.Length; i++) {
                        for (int j = 0; j < floors[0].Length; j++) {
                            if (i < 2 && j < 4) {
                                floors[i][j][0] = 1;
                                floors[i][j][1] = 2;
                            }
                        }
                    }
                    floors[2][1][0] = 1;
                    floors[2][1][1] = 2;
                    floors[2][2][0] = 1;
                    floors[2][2][1] = 2;
                } else if (randomint == 4) {
                    for (int i = 0; i < floors.Length; i++) {
                        for (int j = 0; j < floors[0].Length; j++) {
                            if (i < 3 && j < 3) {
                                floors[i][j][0] = 1;
                                floors[i][j][1] = 2;
                            }
                        }
                    }
                    floors[3][1][0] = 1;
                    floors[3][1][1] = 2;
                } else if (randomint == 5) {
                    for (int i = 0; i < floors.Length; i++) {
                        for (int j = 0; j < floors[0].Length; j++) {
                            if (i < 3 && j < 3) {
                                floors[i][j][0] = 1;
                                floors[i][j][1] = 2;
                            }
                        }
                    }
                    floors[1][3][0] = 1;
                    floors[1][3][1] = 2;
                }
            }


            print();
        }

        /*
         * Generates a 2 story House
         */

        public void generateMultiStoryHouse() {

        }

        /*
         * Generates a Warehouse (2 Stories)
         */

        public void generateWarehouse() {
            if (size == 8) {

            } else if (size == 9) {

            } else if (size == 10) {

            } else if (size == 12) {

            } else if (size == 15) {

            }
        }

        public void test() {

        }

        /*
         * Just a pointless simplification
         */

        public int generate(int min, int max) {
            int randomInt = random.Next(min, max + 1); // strange max
            return randomInt;
        }

        /*
         * Prints the (Ware-)House
         */

        public void print() {
            String temp = "";
            Console.WriteLine("Generated House :");
            Console.WriteLine("");
            Console.WriteLine("0 = nothing");
            Console.WriteLine("1 = room");
            Console.WriteLine("2 = roof");
            Console.WriteLine("3 = balcony? (not implemented yet)");

            for (int i = 0; i < floors[0][0].Length; i++) {

                Console.WriteLine("");
                Console.WriteLine("Floor : " + (i + 1));
                Console.WriteLine("");

                for (int j = 0; j < floors[0].Length; j++) {
                    for (int k = 0; k < floors.Length; k++) {
                        temp += floors[k][j][i];
                    }

                    Console.WriteLine(temp);
                    temp = "";
                }
            }
            Console.WriteLine("");
        }

        /*
         * Initialises the Array
         * x * y * z
         */

        public void initArray(int x, int y, int z) {
            floors = new int[x][][];
            for (int i = 0; i < x; i++) {
                floors[i] = new int[y][];
                for (int j = 0; j < y; j++) {
                    floors[i][j] = new int[z];
                }
            }
        }

    }
}